#!/bin/bash

#	pngtojpg: Copies all the png files from a source dir
#	to a destination dir that are user specified in cmd.
#	It then converts all the png files in the destinati-
#	on directory to jpg files using  mogrify

#	How to Use it: cd to the location of the bash script
#	Enter ./ the name  of the file  followed by the 
#	source and destination directories e.g
#	cd /~Home/Downloads/pngtojpg.sh ~/Src ~/Dst

#The number of arguments must always be two, we have to check for this first
#as the next check depends on there being exactly 2 arguments
if [ ! $# -eq 2 ]; then 
	echo "Arg Error: Format = CMD [source] [destination]"
	exit
fi

#If the first argument (the source directory) doesn't exist then exit and print error
if [ ! -d "$1" ]; then
	echo "Dir Error: unable to open source directory/does not exist"
	exit
fi

#If the second argument (the destination directory) doesn't exist then exit and
#print error
if [ ! -w "$2" ]; then
	echo "Dir Error: unable to write to destination directory"
	exit
fi

#To start set pwd to source directory
cd $1

#Copy png files from source directory to end directory and report number of blocks sent
find . -name "*.png" | cpio -pd --quiet $2

#Set pwd to destination directory
cd $2

#Convert png files to jpg files and then delete the png files. Also updates the user on progress
find . -name "*.png" | while read filename; 
	do 
		if [ ! -d "$filename" ]; then
			mogrify -format jpg "$filename" && rm "$filename";
			echo "Converted "$filename" to "${filename%.*}.jpg""
		fi
	done
